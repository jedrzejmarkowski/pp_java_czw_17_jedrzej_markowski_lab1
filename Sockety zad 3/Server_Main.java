package server_jacek_placek;

import java.io.*;
import java.net.*;
import java.util.*;

public class Server_Main{

    private static final int PORT = 50000;
    private static int glosy_jacek=0;
    private static int glosy_placek=0;
    private static ServerSocket ss;
    private static int odp;
    private static int flaga;

    public static void main(String[] args) throws IOException, ClassNotFoundException{


    	System.out.println("Otwieranie polaczenia...");
    	try {
    		ss = new ServerSocket(PORT);
    	}
    	catch(IOException ioex){
    		System.out.println("Nie nawiazano");
    		System.exit(1);
    	}
    	wykon();

    }
    private static void wykon(){
    	Socket link = null;
    	try{
    	link = ss.accept();
		System.out.println("Nawiazano polaczenie");
        Scanner input = new Scanner(link.getInputStream());
        PrintWriter output = new PrintWriter(link.getOutputStream(), true);
		flaga=0;
    	while(flaga!=-1){
    		odp=input.nextInt();
    		flaga=input.nextInt();
    	if(odp==0){
    		glosy_jacek++;
        	System.out.println("oddalismy glos na jacka , ich ilosc jest rowna "+glosy_jacek);
        	odp=2;
    	}
    	if(odp==1){
    		glosy_placek++;
            System.out.println("oddalismy glos na placka , ich ilosc jest rowna: "+glosy_placek);
            odp=2;
    	}
    	}
    input.close();}
     catch (IOException e) {
        e.printStackTrace();
    }
    finally {
        try {
            System.out.println("Zamykanie polaczenia");
            link.close();
        }
        catch (IOException ie)
        {
            System.out.println("Niepowodzenie polaczenia");
            System.exit(1);

        }

    }
    }
}
