package klient_jacek_placek;
import java.net.*;
import java.util.Scanner;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class Klient_main{
    private static final int PORT = 50000;
    private static final String HOST = "localhost";
    private static JButton Jacek;
    private static JButton Placek;
    private static JButton zakoncz;
    private static int APPLET_WIDTH = 300;
	private static int APPLET_HEIGHT=100;
    private static PrintWriter output;
	public static void main(String[] args) throws IOException, IOException{
		Socket sc = new Socket(HOST,PORT);
		

        output = new PrintWriter(sc.getOutputStream(), true);	
        output.println(2);//dana
        output.println(10);//flaga
    	Jacek = new JButton ("Glosuj na Jacka!");
    	Placek = new JButton ("Glosuj na Placka!");
    	zakoncz = new JButton ("zakoncz");
    	
    	
    	JFrame window = new JFrame("Klient");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel content = new JPanel();
        content.setLayout(null);
        content.setBackground(Color.WHITE);
		content.setOpaque(true);
		Jacek.setLocation(10, 10);
		Jacek.setSize(170, 100);
		Placek.setLocation(200, 10);
		Placek.setSize(170, 100);
		zakoncz.setLocation(200, 150);
		zakoncz.setSize(100, 50);
    	content.add (Jacek);
    	content.add (Placek);
    	content.add (zakoncz);
		window.setVisible(true);
		window.setSize(500, 250);
		window.setContentPane(content); 
	Jacek.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent event)
        {
            output.println(0);//dana
            output.println(10);//flaga
        }});
	Placek.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent event)
        {
            output.println(1);//dana
            output.println(10);//flaga
        }});
	zakoncz.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent event)
        {
            output.println(3);//dana
            output.println(-1);//flaga
            System.exit(0);
        }});
	}
}

