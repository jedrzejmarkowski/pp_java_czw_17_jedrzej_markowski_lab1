﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ConsoleApplication2
{
    class Program
    {
        static public int wielkosc = 30;
        static public int[,] array6 = new int[wielkosc, wielkosc];
        static public int[,] flaga = new int[wielkosc, wielkosc];
        static public void zerowanie_flag()
        {
            for (int i = 1; i < wielkosc - 1; i++)
            {
                for (int j = 1; j < wielkosc - 1; j++)
                {
                    flaga[i, j] = 0;
                }
            }
        }
        static public void inicjacja()
        {
            Random rnd = new Random();
            for(int i = 1; i < wielkosc-1; i++)
            {
                for(int j = 1; j < wielkosc-1; j++)
                {
                    array6[i, j] = rnd.Next(2);
                }
            }
        }
        static public void zmien()
        {

            int licznik = 0;
            zerowanie_flag();
            for (int i = 1; i < wielkosc-1; i++)
            {
                for(int j = 1; j < wielkosc-1; j++)
                {
                    licznik = array6[i - 1, j] + array6[i - 1, j - 1] + array6[i - 1, j + 1] +
                        array6[i, j - 1] + array6[i, j + 1] +
                        array6[i + 1, j - 1] + array6[i + 1, j] + array6[i + 1, j + 1];
                    if (array6[i, j] == 1)//gdy zyje
                    {
                        if (licznik < 2) { flaga[i, j] = 1; }//osamotnienie
                        if (licznik > 3) { flaga[i, j] = 1; }//przeludnienie
                    }
                    if (array6[i, j] == 0)//gdy zmarl
                    {
                        if (licznik == 3) { flaga[i, j] = 1; }//reprodukcja
                    }
                }
            }
            for(int i = 1; i < wielkosc-1; i++)
            {
                for(int j = 1; j < wielkosc-1; j++)
                {
                    if (flaga[i, j] == 1)
                    {
                        if (array6[i, j] == 0) { array6[i, j] = 1; flaga[i, j] = 0; }
                    }
                    if (flaga[i, j] == 1)
                    {
                        if (array6[i, j] == 1) { array6[i, j] = 0; flaga[i, j] = 0; }
                    }
                }
            }
        }
        static void Main(string[] args)
        {
            inicjacja();
            /*
            array6[3, 3] = 1;
            array6[2, 3] = 1;
            array6[4, 3] = 1;
            array6[4, 2] = 1;
            array6[3, 1] = 1;*/
            int a = 0;
            while (a==0)
            {
                for (int i = 1; i < wielkosc-1; i++)
                {
                    for (int j = 1; j < wielkosc-1; j++)
                    {
                        Console.Write(array6[i, j]);
                    }
                    Console.WriteLine();
                }
                Console.ReadLine();
                Console.Clear();
                zmien();
            }
        }
    }
}
