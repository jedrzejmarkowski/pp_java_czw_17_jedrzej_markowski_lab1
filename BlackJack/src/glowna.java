import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.event.*;
class Karta{
	private String[] kolor={"Trefl","Karo","Kier","Pik"};
	private String[] figura={"2","3","4","5","6","7","8","9","10","Walet","Dama","Krol","As"};
	private int[] wartosci={2,3,4,5,6,7,8,9,10,10,10,10,11};
	private int[][] zajete=new int[4][13];
	public String getkolor(int a){return kolor[a];}
	public String getfigura(int a){return figura[a];}
	public int getwartosc(int a){return wartosci[a];}
	public int[] losuj(){
		//funkcja losujaca nowe karty, wyjscie-tablica
		Random generator = new Random();
		int a,b;
		a=generator.nextInt(4);//generowanie 0-3
		b=generator.nextInt(13);//generowanie 0-12
		while(zajete[a][b]!=0)
		{
			a=generator.nextInt(4);//generowanie 0-3
			b=generator.nextInt(13);//generowanie 0-12	
		}
		int[] c={a,b,wartosci[b]};
		zajete[a][b]=1;
		return c;
	}
	public void zerowanie(){
		//funkcja zeruj�ca karty, wchodz� do stosu kart po zakonczonej turze
		for(int i=0;i<4;i++){
			for(int k=0;k<13;k++){
				zajete[i][k]=0;
			}
		}
	}
}
class zaklady{
	private int hajs = 1000;
	private int pula = 0;
	public void zaklad(){
		pula=pula+100;
		hajs=hajs-50;
	}
	public String wyswietl_pieniadze(){
		return "Twoje pieniadze: "+hajs+ "Pula:" + pula;
	}
	public void wygrana(){
		hajs=hajs+pula;
		pula=0;
	}
	public void przegrana(){
		pula=0;
	}
}
class gracz{
	private int[] punkty={0,0};
	public int flaga=0;
	private int[] as={0,0};
	public int wartosc_ukryta=0;
	public String karta_ukryta="";
	private List<String> uklad_gracz = new ArrayList<String>(); // wszystkie karty gracza
	private List<String> uklad_krupier = new ArrayList<String>(); // wszystkie karty krupiera
	public void dodaj_punkty(int a,int gracz){//gracz 0 = krupier, gracz 1 = gracz
		punkty[gracz]=punkty[gracz]+a;
		if(a==11){
			as[gracz]=1;
		}
	}
	public void as_jedynka(){
		for(int i=0;i<2;i++){
		if(as[i]==1 && punkty[i]>21){
			as[i]=0;
			punkty[i]=punkty[i]-10;
        	System.out.println("Zmiana asa na jedynke");
			}
		}
	}
	public int getpunkty(int gracz){//gracz 0 = krupier, gracz 1 = gracz
		return punkty[gracz];
	}
	public void dodaj_do_ukladu(int gracz, String s){//gracz 0 = krupier, gracz 1 = gracz
		if(gracz==1){uklad_gracz.add(s);}
		if(gracz==0){uklad_krupier.add(s);}
	}
	public void zerowanie(){
	uklad_gracz.clear();
	uklad_krupier.clear();
	}
	public void zer_punkty(){
		punkty[0]=0;
		punkty[1]=0;
		as[0]=0;
		as[1]=0;
		flaga=0;
		wartosc_ukryta=0;
		karta_ukryta="";
	}
	public void wyswietl(){//wyswietlanie calego ukladu
        	System.out.println("Twoje karty:");
			for(int i=0;i<uklad_gracz.size();i++)
			{
            	System.out.println(uklad_gracz.get(i));
			}System.out.println("");
        	System.out.println("Karty Krupiera:");
			for(int i=0;i<uklad_krupier.size();i++)
			{
            	System.out.println(uklad_krupier.get(i));
			}
    	System.out.println("------------------------------------------------");
	}
	public String get_uklad_gracz(){
		String s="Uklad gracza:";
		for(int i=0;i<uklad_gracz.size();i++)
		{
			s=s+"  "+uklad_gracz.get(i);
		}
			return s;
	}
	public String get_uklad_krupier(){
		String s="Uklad kurpiera:";
		for(int i=0;i<uklad_krupier.size();i++)
		{
			s=s+"  "+uklad_krupier.get(i);
		}return s;
	}
	
}
class dzialanie{
	public int []tablica_W = {0,0};	
	public void hit_gracz(Karta k, gracz g){//funkcja, hit dla gracza
		int[] tab; // tymczasowa tablica zmiennych
		tab=k.losuj();
		String s=k.getfigura(tab[1])+" "+k.getkolor(tab[0]);
		g.dodaj_punkty(tab[2], 1);
		g.dodaj_do_ukladu(1,s);
		g.as_jedynka();
		
	}
	public int hit_krupier(Karta k,gracz g){
		//funkcja dla krupiera(automat), wyjscia:
		//1-krupier dobral karte
		//0-krupier odpuscil pkt>16
		if(g.getpunkty(0)<17){
			g.flaga++;
		int[] tab; // tymczasowa tablica zmiennych
		tab=k.losuj();
		if(g.flaga!=1){
		String s=k.getfigura(+tab[1])+" "+k.getkolor(tab[0]);
		g.dodaj_punkty(tab[2], 0);
		g.dodaj_do_ukladu(0,s);
		g.as_jedynka();
		}
		else{
			tab=k.losuj();
			g.karta_ukryta=k.getfigura(+tab[1])+" "+k.getkolor(tab[0]);
			g.wartosc_ukryta=tab[2];
			g.as_jedynka();
		}
		return 1;
		}
		else{
		return 0;
		}
	}
	public void dodaj_ukryta(gracz g){
		g.dodaj_do_ukladu(0,g.karta_ukryta);
		g.dodaj_punkty(g.wartosc_ukryta, 0);
	}
	public String podsumowanie(gracz g,zaklady z){
		String s;
    	s=("Podsumowanie! Punkty gracza: "+g.getpunkty(1)+" punkty krupiera: "+g.getpunkty(0)+", ");
    	if(g.getpunkty(0)==g.getpunkty(1)){s=s+("wygral krupier");
    	tablica_W[0]++;z.przegrana();}
    	else if(g.getpunkty(1)>21){s=s+("wygral krupier");tablica_W[0]++;z.przegrana();}
    	else if(g.getpunkty(1)>g.getpunkty(0)){s=s+(" wygral gracz");tablica_W[1]++;z.wygrana();}
    	else if(g.getpunkty(0)>g.getpunkty(1) && g.getpunkty(0)<=21){s=s+("wygral krupier");tablica_W[0]++;z.przegrana();}
    	else{s=s+("wygral gracz");tablica_W[1]++;z.wygrana();}
    	g.zer_punkty();
		g.zerowanie();
		return s;
	}
	
}
public class glowna {
	public static void main(String[] args){
		int a=0;
		dzialanie d=new dzialanie();
		Karta k=new Karta();
		gracz g=new gracz(); 
		zaklady z=new zaklady();
		Pk(g,d,k,z);
		k.zerowanie();
		d.hit_gracz(k, g);
		d.hit_krupier(k, g);
		d.hit_gracz(k, g);
		d.hit_krupier(k, g);
	}
	public static void Pk(gracz g, dzialanie d, Karta k,zaklady z){
		JFrame window = new JFrame("BlackJack");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel content = new JPanel();
		content.setOpaque(true);
        content.setBackground(Color.WHITE);
        content.setLayout(null);
		
		JLabel label_gracz = new JLabel(g.get_uklad_gracz()); 
		label_gracz.setSize(500,70);
		label_gracz.setLocation(5,50);
		
		JLabel label_krupier = new JLabel(g.get_uklad_krupier());
		label_krupier.setSize(500,70);
		label_krupier.setLocation(5,100);
		
		JLabel podsumowanie = new JLabel("");
		podsumowanie.setSize(500,70); 
		podsumowanie.setLocation(5,200);
		
		JLabel zaklad = new JLabel("");
		zaklad.setSize(500,70); 
		zaklad.setLocation(5,300);
		
		window.setVisible(true);
		window.setSize(700, 400);
		
		JButton button_hit = new JButton("hit");
        button_hit.setSize(80, 50);
        button_hit.setLocation(5,5);
        
		JButton	button_pass = new JButton("stick");
        button_pass.setSize(80, 50);
        button_pass.setLocation(100,5);
/*		
		d.hit_krupier(k, g);
        System.out.println(g.get_uklad_krupier());
		d.hit_krupier(k, g);
        System.out.println(g.get_uklad_krupier());*/
		content.add(button_hit);
		content.add(button_pass);
		content.add(label_gracz);
		content.add(label_krupier);
		content.add(podsumowanie);
		content.add(zaklad);
		window.setContentPane(content);
		button_hit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				d.hit_gracz(k, g);
				label_gracz.setText(g.get_uklad_gracz());
				label_krupier.setText(g.get_uklad_krupier());
			}
		});
		button_pass.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
	            System.out.println(g.get_uklad_krupier());
				d.dodaj_ukryta(g);
	            System.out.println(g.get_uklad_krupier());
				d.hit_krupier(k, g);
	            System.out.println(g.get_uklad_krupier());
				d.hit_krupier(k, g);
	            System.out.println(g.get_uklad_krupier());
				d.hit_krupier(k, g);
				podsumowanie.setText(d.podsumowanie(g,z));
				k.zerowanie();
				d.hit_gracz(k, g);
				d.hit_krupier(k, g);
				d.hit_gracz(k, g);
				d.hit_krupier(k, g);
				z.zaklad();
				zaklad.setText(z.wyswietl_pieniadze());
				label_gracz.setText(g.get_uklad_gracz());//odswiezenie
				label_krupier.setText(g.get_uklad_krupier());
	            System.out.println(d.tablica_W[0] + " <-krupier gracz-> " +d.tablica_W[1]);
			}
		});
	}
}
