import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import java.awt.*;
import java.awt.event.*;
class Gracz{
	private int[] gracze= {-1,1};
	public void wygrana(int a){gracze[a]++;}
	public int get_gracz(int a){return gracze[a];}
}

class Karty{
	private List<Integer> kolory = new ArrayList<Integer>();
	private List<Integer> figury = new ArrayList<Integer>();
	public void setkolor(int A){kolory.add(A);}
	public void setfigura(int A){figury.add(A);}
	public int getkolor(int A){return kolory.get(A);}
	public int getfigura(int A){return figury.get(A);}
	public void kasowanie(){kolory.clear();figury.clear();}
	public void sortowanie(){
		int[] tmp1=new int[5];int[] tmp2=new int[5];int[] tmp3=new int[10];int[] tmp4=new int[10];
		for(int i=0;i<5;i++){tmp1[i]=getfigura(i);tmp2[i]=getkolor(i);}
		for(int i=5;i<10;i++){tmp3[i]=getfigura(i);tmp4[i]=getkolor(i);}
		kasowanie();Arrays.sort(tmp1);Arrays.sort(tmp2);Arrays.sort(tmp3);Arrays.sort(tmp4);//sortowanie
		for(int i=0;i<5;i++){setfigura(tmp1[i]);setkolor(tmp2[i]);}
		for(int i=5;i<10;i++){setfigura(tmp3[i]);setkolor(tmp4[i]);}
	}
	public void pokaz(){for(int i=0;i<10;i++){//wyswietlenie calej talii
        System.out.print(getfigura(i) + " ");
	}System.out.println("");}
}

class Sprawdzenie {
	private int[] wartosc_ukladu=new int[2]; // wartosc ukladu dla gracza 1 i 2
	private int[] wartosc_kart=new int[2]; // wartosc kart dla gracza 1 i 2
	private int[] wartosc_2par=new int[2]; // wartosc w przypadku dwoch par, fulla
	public void wrzutka(int p,int nr_gracza,int wartosc_karty1, int wartosc_karty2){
		if(p==1){wartosc_ukladu[nr_gracza]=1;wartosc_kart[nr_gracza]=wartosc_karty1;
		wartosc_2par[nr_gracza]=wartosc_karty2;}//na wypadek wysokiej karty
		if(p==2){wartosc_ukladu[nr_gracza]=2;wartosc_kart[nr_gracza]=wartosc_karty1;} //na wypadek pary
		if(p==4){wartosc_ukladu[nr_gracza]=3;wartosc_kart[nr_gracza]=wartosc_karty1;
		wartosc_2par[nr_gracza]=wartosc_karty2;}//na wypadek 2 par
		if(p==6){wartosc_ukladu[nr_gracza]=4;wartosc_kart[nr_gracza]=wartosc_karty2;}//na wypadek trojki
		if(p==8){wartosc_ukladu[nr_gracza]=7;wartosc_kart[nr_gracza]=wartosc_karty1;
		wartosc_2par[nr_gracza]=wartosc_karty2;}//na wypadek fula
		if(p==12){wartosc_ukladu[nr_gracza]=8;wartosc_kart[nr_gracza]=wartosc_karty1;}//na wypadek karety
		if(p==30){wartosc_ukladu[nr_gracza]=5;wartosc_kart[nr_gracza]=wartosc_karty1;}//na wypadek strita
		if(p==50){wartosc_ukladu[nr_gracza]=6;wartosc_kart[nr_gracza]=wartosc_karty1;}//na wypadek koloru
		if(p==100){wartosc_ukladu[nr_gracza]=9;wartosc_kart[nr_gracza]=wartosc_karty1;//na wypadek pokera
			if(wartosc_karty1==62){wartosc_ukladu[nr_gracza]=10;wartosc_kart[nr_gracza]=wartosc_karty1;}}//poker krolewski
	}
	public void czy_wysoka(Karty T){
		wrzutka(1,0,T.getfigura(4),T.getfigura(3));
		wrzutka(1,1,T.getfigura(9),T.getfigura(8));
	}
	public void czy_pary(Karty T){//sprawdzenie czy s� pary
		int parki=0;int stala1=0;int stala2=0;int tmp1;int tmp2=0;int tmp3;
		for(int i=0;i<5;i++){
			for(int j=0;j<5;j++){
				if(j!=i){
					if(T.getfigura(j)==T.getfigura(i)){
						parki++;
						tmp1=T.getfigura(j);
						if(tmp1!=tmp2 && tmp2!=0){stala2=tmp1;}
						tmp2=tmp1;
						if(stala1==0){stala1=tmp1;}}//sprawdzanie pierwszego gracza
					if(stala1<stala2){tmp3=stala1;stala1=stala2;stala2=tmp3;} // gdy 2 pary
				}}}wrzutka(parki,0,stala1,stala2);
				parki=0;stala1=0;stala2=0;tmp1=0;tmp2=0;//zerowanie
		for(int i=5;i<10;i++){
			for(int j=5;j<10;j++){
				if(j!=i){
					if(T.getfigura(j)==T.getfigura(i)){
						parki++;
						tmp1=T.getfigura(j);
						if(tmp1!=tmp2 && tmp2!=0){stala2=tmp1;}
						tmp2=tmp1;
						if(stala1==0){stala1=tmp1;}}//sprawdzanie drugiego gracza
					if(stala1<stala2){tmp3=stala1;stala1=stala2;stala2=tmp3;} // gdy 2 pary
				}}}wrzutka(parki,1,stala1,stala2);}
	public void czy_strit_kolor(Karty T){
		int tmp1=0;int tmp2=0;
		for(int i=0;i<4;i++){
			if(T.getfigura(i+1)-T.getfigura(i)==1){tmp1++;}
		}if(tmp1==4){
			wrzutka(30,0,T.getfigura(4),0);} // pierwszy gracz strit
			if(T.getfigura(0)==50 && T.getfigura(1)==51 && T.getfigura(2)==52 && T.getfigura(3)==53 && 
					T.getfigura(4)==62){wrzutka(30,0,T.getfigura(3),0);tmp1=4;} // na wypadek A2345
		for(int i=0;i<4;i++){
			if(T.getkolor(i+1)-T.getkolor(i)==0){tmp2++;} // pierwszy gracz kolor
		}if(tmp2==4){wrzutka(50,0,T.getfigura(4),0);}
		if(tmp1==4 && tmp2==4){wrzutka(100,0,T.getfigura(4),0);}//pierwszy gracz poker
		tmp2=0;tmp1=0;
		for(int i=5;i<9;i++){
			if(T.getfigura(i+1)-T.getfigura(i)==1){tmp1++;}
		}if(tmp1==4){wrzutka(30,1,T.getfigura(9),0);}//drugi gracz strit
		if(T.getfigura(5)==50 && T.getfigura(6)==51 && T.getfigura(7)==52 && T.getfigura(8)==53 && 
				T.getfigura(9)==62){wrzutka(30,1,T.getfigura(8),0);tmp1=4;} // na wypadek A2345
		for(int i=5;i<9;i++){
			if(T.getkolor(i+1)-T.getkolor(i)==0){tmp2++;}
		}if(tmp2==4){wrzutka(50,1,T.getfigura(9),0);}//drugi gracz kolor
		if(tmp1==4 && tmp2==4){wrzutka(100,1,T.getfigura(9),0);}//drugi gracz poker
	}
	public void zerowanie(){for(int i=0;i<2;i++){ // funkcja zeruj�ca
		wartosc_ukladu[i]=0;wartosc_kart[i]=0;wartosc_2par[i]=0;}}
	public int Czywygrana(Karty T){//sprawdzenie kto wygral, wyjscie 0 1
		if(wartosc_ukladu[0]>wartosc_ukladu[1]){return (0);}
		else if(wartosc_ukladu[0]<wartosc_ukladu[1]){return (1);}
		else{
			if(wartosc_kart[0]>wartosc_kart[1]){return (0);}
			else if(wartosc_kart[1]>wartosc_kart[0]){return (1);}
			else{if(wartosc_ukladu[0]==1){T.pokaz();}
				if(wartosc_2par[0]>wartosc_2par[1]){return (0);}
				else if(wartosc_2par[1]>wartosc_2par[0]){return (1);}
				else{System.out.println("brak wygranego, koniec, blad"); return 0;}}}//blad gdy brak wygranego
	}
}

class Pk extends JFrame{
    TextField text = new TextField(50);
	JPanel p=new JPanel();
	JButton b=new JButton("Zacznij");
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static void main(String[] args){
		Gracz G=new Gracz();
		Wczytywanie W=new Wczytywanie();
		new Pk(G,W);
	}
	public Pk(Gracz G, Wczytywanie W){
		super("Poker");
		setSize(300,200);
		setResizable(true);
		setVisible(true);
		p.add(b);
		add(p);
		add(text);
		W.odczytajPlik(G);
        text.setText("Gracz 1 wygral " + G.get_gracz(0) + " razy, a Gracz 2 wygral " + G.get_gracz(1)+ " razy");
	}
}
	class Wczytywanie{
	public void odczytajPlik(Gracz G) {
		String odczyt = "";
		int y=0;
		int x=0;
		int b;
		int[] gracze={0,0};
		Sprawdzenie S=new Sprawdzenie();
		Karty k=new Karty();
		gracze[0]--;gracze[1]++;
		try {
    		FileInputStream fileInputStream = new FileInputStream("poker.txt");
    		b=1;
    		while(b != -1){
    			b = fileInputStream.read();
    			if(b==67 || b==68 || b==72 || b==83){k.setkolor(b);y++;}
    			else if(b!=32 && b!=13 &&b!=10){
    				if(b==65){b=62;}//As
    				if(b==75){b=61;}//Krol
    				if(b==81){b=60;}//Dama
    				if(b==74){b=59;}//Jopek
    				if(b==84){b=58;}//Dziesiec
    				k.setfigura(b);x++;}
    			if(x==10 && y==10){
    				y=0;x=0;
    				S.zerowanie();
    				k.sortowanie();
    				S.czy_wysoka(k);
    				S.czy_pary(k);
    				S.czy_strit_kolor(k);
    				gracze[S.Czywygrana(k)]++;
    				G.wygrana(S.Czywygrana(k));
    				k.kasowanie();
    			}
    		}
            System.out.println(gracze[0]);
        } catch (FileNotFoundException e) {
            System.out.println("Brak Pliku do odczytania!");
        }
        catch (IOException e) {
        	   e.printStackTrace();
        }
	}
	
	
}